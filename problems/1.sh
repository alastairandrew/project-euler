#!/bin/bash
# A solution to Problem 1 of Project Euler
# By Alastair Andrew
#

N=${1:-1000}

sum1toN () {
	echo "$1 * ($1 + 1) / 2" | bc
}

sumMultiples () {
	arg=$(echo "(($1 - 1) / $2)" | bc)
	echo "$(sum1toN $arg) * $2" | bc
}

threes=$(sumMultiples $N 3)
fives=$(sumMultiples $N 5)
fifteens=$(sumMultiples $N 15)

echo $((threes + fives - fifteens))
