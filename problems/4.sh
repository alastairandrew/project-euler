#!/bin/bash
# A solution to Problem 4 of Project Euler
# By Alastair Andrew
#

function isPalindrome {
	if [ ${#1} -eq 1 ]; then
		return 0
	elif [ ${1:0:1} -eq ${1: -1} ]; then
			local trimmedNo=${1:1:$((${#1}-2))}
			if [ ${#trimmedNo} -eq 0 ]; then
				return 0
			fi
			isPalindrome $trimmedNo
	else
		return 1
	fi
}

maxPalindrome=0
for (( i=999; i >= 100; i--)); do
	if [ $((i * i)) -lt $maxPalindrome ]; then
		continue
	fi
	for (( j=i; j >= 100 && (j*i) > $maxPalindrome; j--)); do
		value=$((i * j))
		if isPalindrome $value -a [ $maxPalindrome -lt $value ] ; then 
			maxPalindrome=$value
		fi
	done
done
echo $maxPalindrome