#!/bin/bash
# A solution to Problem 3 of Project Euler
# By Alastair Andrew
#

N=${1:-600851475143}

M=$N

for ((i=2; i**2<M; i++)); do 
	if ((M % i == 0)); then
		((M/=i))
	fi
	if ((M <= i)); then
		break;
	fi
done 

echo $M
