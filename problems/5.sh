#!/bin/bash
# 5.sh
# 
#
# Created by Alastair Andrew on 30/11/2011.
# Copyright 2011 University of Strathclyde. All rights reserved.
# What is the smallest positive number that is evenly divisible by all the numbers 1 to 20?

for i in {1..2000}; do
	echo $i
done
