#!/bin/bash
# A solution to Problem 2 of Project Euler
# By Alastair Andrew
#

MAX_TERM_SIZE=${1:-4000000}

fibonacci () {
	back2=0
	back1=1
	next=0
	evenSum=0
	evens=0

	for ((x=2; next<$MAX_TERM_SIZE; x++)); do
		next=$((back1 + back2))
		if ((next % 2 == 0)); then
			((evenSum+=next))
			((evens++))
		fi		
		back2=$back1
		back1=$next
		#echo "F_$x = $next back1:$back1 back2:$back2 evenSum:$evenSum evens:$evens"
	done
	echo "There were $evens even nos in the sum"
	echo $evenSum
}

fibonacci